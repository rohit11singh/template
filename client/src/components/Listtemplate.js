import axios from 'axios';
import React,{useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

const Listtemplate = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios.get('http://localhost:5000/').then(function (response) {
      setData(response.data);
          }).catch(function (error) {
            console.log(error);
    });
},[]);
  const handleClick = (e) => {
    e.preventDefault();
    axios.get('http://localhost:5000/').then(function (response) {
        setData(response.data);
          }).catch(function (error) {
            console.log(error);
    });
  }
  return (
    <div className='list-btn'>
        <button className='btn' type='button' onClick={handleClick}>Refresh List</button>
        <div className='data-wrapper'>
        {data.map(item => (
          <Link to={`/gettemplate/${item.Name}`} key={item.Name}  style={{ textDecoration: 'none' }}>
            <div key={item.Name} className='data-container'>
            <div className='data-inner'>Template Name:  {item.Name}   Created at:  {item.CreatedTimestamp}</div>
          </div>
          </Link>
        ))}
    </div>
    </div>
  );
};

export default Listtemplate;