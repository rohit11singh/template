import React from 'react';
import { Link } from 'react-router-dom';
import "./Navbar.css";

const Navbar = () => {
  return(
      <nav className='navbar'>
          <div className='nav-link'>
              <Link to='/' className='template'>
                  <div className='list-item'>Create Template</div>
              </Link>
              <Link to='/listtemplate' className='template'>
                  <div className='list-item'>List Template</div>
              </Link>
          </div>
      </nav>
  );
};

export default Navbar;
