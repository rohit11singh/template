import React, { useState } from "react";


const Login = (props) => {
	const [userName, setUserName] = useState("");
	const [password, setPassword] = useState("");
	const handleUserNameChange = (e) => {
		setUserName(e.target.value);
	};
	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
	};
	const handleSubmit = (e) => {
		setUserName("");
		setPassword("");
		if (
			userName === "username@brighchamps.com" &&
			password === "password@brighchamps"
		) {
			props.autherizer(true);
		}
	};
	return (
		<>
			<div className="input-wrapper">
				<div className="input-container">
					<h2>Login to Create Template</h2>
				</div>
				<div className="input-container">
					<input
						type="text"
						className="input-template-name"
						placeholder="User Name"
						value={userName}
						onChange={handleUserNameChange}
					></input>
				</div>
				<div className="input-container">
					<input
						type="password"
						className="input-template-name"
						placeholder="Password"
						value={password}
						onChange={handlePasswordChange}
					></input>
				</div>
				<div>
					<button className="btn" type="button" onClick={handleSubmit}>
						Login
					</button>
				</div>
			</div>
		</>
	);
};

export default Login;
