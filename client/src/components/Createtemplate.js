import axios from "axios";
import React, { useState } from "react";

const Createtemplate = () => {
	const [templateName, setTemplateName] = useState("");
	const [htmlInput, setHtmlInput] = useState("");
	const [textInput, setTextInput] = useState("");
	const [subjectValue, setSubjectValue] = useState("");
	const handleTemplateNameChange = (e) => {
		setTemplateName(e.target.value);
	};
	const handleSubjectChange = (e) => {
		setSubjectValue(e.target.value);
	};
	const handleHtmlChange = (e) => {
		setHtmlInput(e.target.value);
	};
	const handleTextChange = (e) => {
		setTextInput(e.target.value);
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		const data = {
			template: templateName,
			html_content: htmlInput,
			subject: subjectValue,
			text: textInput,
		};
		axios
			.post("http://localhost:5000/", data)
			.then(function (response) {
				alert(response.data);
			})
			.catch(function (error) {
				console.log(error);
			});
		setTemplateName("");
		setSubjectValue("");
		setHtmlInput("");
		setTextInput("");
	};
	return (
		<form className="input-wrapper" onSubmit={handleSubmit}>
			<div className="input-container">
				<input
					type="text"
					className="input-template-name"
					placeholder="Template Name"
					value={templateName}
					onChange={handleTemplateNameChange}
				></input>
			</div>
			<div className="input-container">
				<input
					type="text"
					className="input-subject-name"
					placeholder="Subject"
					value={subjectValue}
					onChange={handleSubjectChange}
				></input>
			</div>
			<div className="input-container">
				<textarea
					className="input-email-body"
					placeholder="HTML Input"
					value={htmlInput}
					onChange={handleHtmlChange}
				></textarea>
			</div>
			<div className="input-container">
				<textarea
					type="text"
					className="input-text"
					placeholder="Text"
					value={textInput}
					onChange={handleTextChange}
				></textarea>
			</div>
			<div>
				<button type="submit" className="btn">
					Create Template
				</button>
			</div>
		</form>
	);
};

export default Createtemplate;
