import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const Gettemplate = () => {
	const { name } = useParams();
	const [htmlName, setHtmlName] = useState("");
	const [toEmail, setToEmail] = useState("");
	const [dataVar, setDataVar] = useState("");
	const [dataVal, setDataVal] = useState("");
	useEffect(() => {
		axios
			.get(`http://localhost:5000/${name}`)
			.then(function (response) {
				setHtmlName(response.data.HtmlPart);
			})
			.catch(function (error) {
				console.log(error);
			});
	}, []);
	const handleToEmailChange = (e) => {
		setToEmail(e.target.value);
	};
	const handleDataVarChange = (e) => {
		setDataVar(e.target.value);
	};
	const handleDataValChange = (e) => {
		setDataVal(e.target.value);
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		const data = {
			toAddresses: toEmail,
			htmlContent: htmlName,
			dataVar: dataVar,
			dataVal: dataVal,
			templateName: name,
		};
		axios
			.post("http://localhost:5000/send", data)
			.then(function (response) {
				console.log(response.data);
			})
			.catch(function (error) {
				console.log(error);
			});
		setToEmail("");
		setDataVar("");
		setDataVal("");
	};
	return (
		<div className="input-wrapper">
			<p>Template Name: {name}</p>
			<div className="input-container">
				<input
					className="input-to-email"
					type="email"
					value={toEmail}
					placeholder="To Email"
					onChange={handleToEmailChange}
				></input>
			</div>
			<div>
				<p>HTML Content</p>
				<textarea
					readOnly
					className="input-email-body"
					value={htmlName}
				></textarea>
			</div>
			<div className="input-container">
				<textarea
					className="input-email-body"
					value={dataVar}
					onChange={handleDataVarChange}
					placeholder="Data Variable with Comma"
				></textarea>
			</div>
			<div className="input-container">
				<textarea
					className="input-email-body"
					value={dataVal}
					onChange={handleDataValChange}
					placeholder="Data Value with Comma"
				></textarea>
			</div>
			<div>
				<button type="submit" className="btn" onClick={handleSubmit}>
					Send Email
				</button>
			</div>
		</div>
	);
};

export default Gettemplate;
