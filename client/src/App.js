import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Createtemplate from "./components/Createtemplate";
import Navbar from "./components/navbar/Navbar";
import Listtemplate from "./components/Listtemplate";
import "./App.css";
import Gettemplate from "./components/Gettemplate";
import Login from "./components/Login";

const App = () => {
	const [auth, setAuth] = useState(false);
	const authHandler = (value) => {
		setAuth(value);
	}
	if(!auth){
		return <Login autherizer={authHandler}/>;
	}
	return (
		<Router>
			<Navbar />
			<Routes>
				<Route
					path="/"
					element={<Createtemplate />}
					exact
				></Route>
				<Route path="/listtemplate" element={<Listtemplate />} exact></Route>
				<Route path="/gettemplate/:name" element={<Gettemplate />}></Route>
				<Route path="/:login" element={<Login />}></Route>
			</Routes>
		</Router>
	);
};

export default App;
