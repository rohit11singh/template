const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
app.use(cors());
app.use(bodyParser.json());
var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });

app.post("/", (req, res) => {
	console.log("received");
	var params = {
		Template: {
			TemplateName: req.body.template,
			HtmlPart: req.body.html_content,
			SubjectPart: req.body.subject,
			TextPart: req.body.text,
		},
	};
	var templatePromise = new AWS.SES({ apiVersion: "2010-12-01" })
		.createTemplate(params)
		.promise();
	templatePromise
		.then(function (data) {
			//   res.json({ msg: "Template created successfully", data: data });
			res.send("Template Created Successfully");
			console.log("Template created successfully");
		})
		.catch(function (err) {
			console.error(err, err.stack);
		});
});

app.post("/send", (req, res) => {
	AWS.config.update({ region: "us-east-1" });
	var toAddresses = req.body.toAddresses.toString();
	var dataVar = req.body.dataVar;
	var dataVal = req.body.dataVal;
	var templateName = req.body.templateName;

	var commaSeparatedEmailsList = toAddresses.split(",");
	var variablesList = dataVar.toString().split(",");
	var valuesList = dataVal.toString().split(",");

	var templateData = generateTemplateData(variablesList, valuesList);

	var params = {
		Destination: {
			/* required */
			/*
      CcAddresses := ToAddresses 
      */
			CcAddresses: [...commaSeparatedEmailsList],
			ToAddresses: [...commaSeparatedEmailsList],
		},
		/*
        Source := first address of the ToAddresses
    */
		Source: commaSeparatedEmailsList[0].toString() /* required */,
		Template: `${templateName}` /* required */,
		TemplateData: `${templateData}` /* required */,
		ReplyToAddresses: [`${commaSeparatedEmailsList[0]}`],
	};

	//   Create the promise and SES service object
	var sendPromise = new AWS.SES({ apiVersion: "2010-12-01" })
		.sendTemplatedEmail(params)
		.promise();

	// Handle promise's fulfilled/rejected states
	sendPromise
		.then(function (data) {
			console.log(data);
			res.json(data);
			// res.send("Mail Sent Successfully");
		})
		.catch(function (err) {
			console.error(err, err.stack);
			res.send(err);
		});
});

function generateTemplateData(variables, values) {
	let templateData = {};
	variables.forEach((variable, index) => {
		templateData[variable] = values[index];
	});
	let convertedTemplateData = JSON.stringify(templateData);
	return convertedTemplateData;
}

app.get("/", (req, res) => {
	var templatePromise = new AWS.SES({ apiVersion: "2010-12-01" })
		.listTemplates({ MaxItems: 100 })
		.promise();
	templatePromise
		.then(function (data) {
			const template = data.TemplatesMetadata;
			res.json(template);
		})
		.catch(function (err) {
			console.error(err, err.stack);
			res.send(err);
		});
});

app.get("/:templateName", (req, res) => {
	var templatePromise = new AWS.SES({ apiVersion: "2010-12-01" })
		.getTemplate({ TemplateName: `${req.params.templateName}` })
		.promise();

	// Handle promise's fulfilled/rejected states
	templatePromise
		.then(function (data) {
			res.json(data.Template);
		})
		.catch(function (err) {
			console.error(err, err.stack);
			res.send(err);
		});
});

app.listen(5000,"localhost", () => console.log(`Server started on port 5000`));
